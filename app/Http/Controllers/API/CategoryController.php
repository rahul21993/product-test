<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use App\Models\Category;

class CategoryController extends Controller {
	
    public function __construct() {}

    public function index(Request $request) {
		
		$categories = Category::getData();
		return response()->json($categories);
			
    }
	
	public function store(Request $request){

		$input = $request->all();

		$validator = Validator::make($input, [
            'name'			=>	'required',
        ]);

        $input = array_map('trim', $input);

        if ($validator->fails()) {
            $message = $validator->messages()->first();
            return response()->json(compact('message'), 400);
        }

        $category = new Category;
        $category->name = $input['name'];
        $category->parent_id = isset($input['parent_id']) && $input['parent_id'] ? $input['parent_id'] : NULL;
        $category->save();

        $data = array(
            'id'		=>	$category->id,
            'message'	=>	"Category Created",
        );
        return response()->json($data);
	}

	public function update($category_id, Request $request) {

        $input = $request->all();
        $validator = Validator::make($input, [
            'name'			=>	'required',
        ]);
        $input = array_map('trim', $input);

        if ($validator->fails()) {
            $message = $validator->messages()->first();
            return response()->json(compact('message'), 400);
        }
		
		$category_id = (int) $category_id;
	
		$category = Category::where('id', $category_id)->where('flag', 1)->first();
        if (is_null($category)) {
            $message = 'Category not found';
            return response()->json(compact('message'), 404);
        }

        $category->name = $input['name'];
        $category->parent_id = isset($input['parent_id']) && $input['parent_id'] ? $input['parent_id'] : NULL;
        $category->save();

        $message = 'Category Updated';
        return response()->json(compact('message'));
    }

    public function show($category_id, Request $request) {

        $category_id = (int) $category_id;
    
        $category = Category::where('id', $category_id)->where('flag', 1)->first();
        if (is_null($category)) {
            $message = 'Category not found';
            return response()->json(compact('message'), 404);
        }

        return response()->json($category);

    }

    public function destroy($category_id, Request $request) {

        $category_id = (int) $category_id;
	
		$category = Category::where('id', $category_id)->where('flag', 1)->first();
        if (is_null($category)) {
            $message = 'Category not found';
            return response()->json(compact('message'), 404);
        }

        $category->flag = 0;
        $category->save();

        $message = 'Category Deleted';
        return response()->json(compact('message'));
    }



}