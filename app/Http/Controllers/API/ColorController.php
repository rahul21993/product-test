<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use App\Models\Color;

class ColorController extends Controller {
	
    public function __construct() {}

    public function index(Request $request) {
		
		$colors = Color::getData();
		return response()->json($colors);

    }
	
	public function store(Request $request){

		$input = $request->all();

		$validator = Validator::make($input, [
            'code'			=>	'required',
            'name'			=>	'required',
        ]);

        $input = array_map('trim', $input);

        if ($validator->fails()) {
            $message = $validator->messages()->first();
            return response()->json(compact('message'), 400);
        }

        $color = new Color;
        $color->code = $input['code'];
        $color->name = $input['name'];
        $color->save();

        $data = array(
            'id'		=>	$color->id,
            'message'	=>	"Color Created",
        );
        return response()->json($data);
	}

	public function update($color_id, Request $request) {

        $input = $request->all();
        $validator = Validator::make($input, [
            'code'			=>	'required',
            'name'			=>	'required',
        ]);
        $input = array_map('trim', $input);

        if ($validator->fails()) {
            $message = $validator->messages()->first();
            return response()->json(compact('message'), 400);
        }
		
		$color_id = (int) $color_id;
	
		$color = Color::where('id', $color_id)->where('flag', 1)->first();
        if (is_null($color)) {
            $message = 'Color not found';
            return response()->json(compact('message'), 404);
        }

        $color->code = $input['code'];
        $color->name = $input['name'];
        $color->save();

        $message = 'Color Updated';
        return response()->json(compact('message'));
    }

    public function show($color_id, Request $request) {

        $color_id = (int) $color_id;
	
		$color = Color::where('id', $color_id)->where('flag', 1)->first();
        if (is_null($color)) {
            $message = 'Color not found';
            return response()->json(compact('message'), 404);
        }

        return response()->json($color);

    }

    public function destroy($color_id, Request $request) {

        $color_id = (int) $color_id;
	
		$color = Color::where('id', $color_id)->where('flag', 1)->first();
        if (is_null($color)) {
            $message = 'Color not found';
            return response()->json(compact('message'), 404);
        }

        $color->flag = 0;
        $color->save();

        $message = 'Color Deleted';
        return response()->json(compact('message'));
    }



}