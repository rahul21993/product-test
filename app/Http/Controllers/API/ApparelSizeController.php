<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use App\Models\ApparelSize;

class ApparelSizeController extends Controller {
	
    public function __construct() {}

    public function index(Request $request) {
		
		$apparel_sizes = ApparelSize::getData();
		return response()->json($apparel_sizes);
			
    }
	
	public function store(Request $request){

		$input = $request->all();

		$validator = Validator::make($input, [
            'code'			=>	'required',
            'sort_order'	=>	'required',
        ]);

        $input = array_map('trim', $input);

        if ($validator->fails()) {
            $message = $validator->messages()->first();
            return response()->json(compact('message'), 400);
        }

        $apparel_size = new ApparelSize;
        $apparel_size->code = $input['code'];
        $apparel_size->sort_order = $input['sort_order'];
        $apparel_size->save();

        $data = array(
            'id'		=>	$apparel_size->id,
            'message'	=>	"Apparel Size Created",
        );
        return response()->json($data);
	}

	public function update($apparel_size_id, Request $request) {

        $input = $request->all();
        $validator = Validator::make($input, [
            'code'			=>	'required',
            'sort_order'	=>	'required',
        ]);
        $input = array_map('trim', $input);

        if ($validator->fails()) {
            $message = $validator->messages()->first();
            return response()->json(compact('message'), 400);
        }
		
		$apparel_size_id = (int) $apparel_size_id;
	
		$apparel_size = ApparelSize::where('id', $apparel_size_id)->where('flag', 1)->first();
        if (is_null($apparel_size)) {
            $message = 'Apparel Size not found';
            return response()->json(compact('message'), 404);
        }

        $apparel_size->code = $input['code'];
        $apparel_size->sort_order = $input['sort_order'];
        $apparel_size->save();

        $message = 'Apparel Size Updated';
        return response()->json(compact('message'));
    }

    public function show($apparel_size_id, Request $request) {

        $apparel_size_id = (int) $apparel_size_id;
    
        $apparel_size = ApparelSize::where('id', $apparel_size_id)->where('flag', 1)->first();
        if (is_null($apparel_size)) {
            $message = 'Apparel Size not found';
            return response()->json(compact('message'), 404);
        }

        return response()->json($apparel_size);

    }

    public function destroy($apparel_size_id, Request $request) {

        $apparel_size_id = (int) $apparel_size_id;
	
		$apparel_size = ApparelSize::where('id', $apparel_size_id)->where('flag', 1)->first();
        if (is_null($apparel_size)) {
            $message = 'Apparel Size not found';
            return response()->json(compact('message'), 404);
        }

        $apparel_size->flag = 0;
        $apparel_size->save();

        $message = 'Apparel Size Deleted';
        return response()->json(compact('message'));
    }



}