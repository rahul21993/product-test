<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use App\Models\Product;
use App\Models\ProductCategory;
use App\Models\ProductColor;

class ProductController extends Controller {

	public function __construct() {}

    public function index(Request $request) {
		
		$products = Product::getData();
		return response()->json($products);

    }

    public function store(Request $request){

		$input = $request->all();

		$validator = Validator::make($input, [
            'name'				=>	'required',
            'categories'		=>	'required',
            'apparel_size_id'	=>	'required|exists:apparel_sizes,id',
        ]);

        $input = array_map('trim', $input);

        if ($validator->fails()) {
            $message = $validator->messages()->first();
            return response()->json(compact('message'), 400);
        }

        $categories = json_decode($input['categories']);
        $colors = !empty($input['colors']) ? json_decode($input['colors']) : NULL;

        $product = new Product;
        $product->name = $input['name'];
        $product->apparel_size_id = $input['apparel_size_id'];
        $product->description = isset($input['description']) && $input['description'] ? $input['description'] : NULL;
        $product->save();

        if(!empty($categories) && count($categories) > 0) {
        	foreach ($categories as $category_id) {
        		$product_category = new ProductCategory;
	        	$product_category->product_id = $product->id;
	        	$product_category->category_id = $category_id;
	        	$product_category->save();
        	}
        }

        if(!empty($colors) && count($colors) > 0) {
        	foreach ($colors as $color_id) {
        		$product_color = new ProductColor;
	        	$product_color->product_id = $product->id;
	        	$product_color->color_id = $color_id;
	        	$product_color->save();
        	}
        }

        $data = array(
            'id'		=>	$product->id,
            'message'	=>	"Product Created",
        );
        return response()->json($data);
	}

	public function update($product_id, Request $request) {

        $input = $request->all();
        $validator = Validator::make($input, [
            'name'				=>	'required',
            'categories'		=>	'required',
            'apparel_size_id'	=>	'required|exists:apparel_sizes,id',
        ]);
        $input = array_map('trim', $input);

        if ($validator->fails()) {
            $message = $validator->messages()->first();
            return response()->json(compact('message'), 400);
        }
		
		$product_id = (int) $product_id;
	
		$product = Product::where('id', $product_id)->where('flag', 1)->first();
        if (is_null($product)) {
            $message = 'Product not found';
            return response()->json(compact('message'), 404);
        }

        $product->name = $input['name'];
        $product->apparel_size_id = $input['apparel_size_id'];
        $product->description = isset($input['description']) && $input['description'] ? $input['description'] : NULL;
        $product->save();

        $categories = json_decode($input['categories']);
        $colors = !empty($input['colors']) ? json_decode($input['colors']) : NULL;
		
		ProductColor::where('product_id', $product_id)->update(array('flag' => 0));

        if(!empty($colors) && count($colors) > 0) {
        	foreach ($colors as $color_id) {
        		$product_color = new ProductColor;
	        	$product_color->product_id = $product->id;
	        	$product_color->color_id = $color_id;
	        	$product_color->save();
        	}
        }

        ProductCategory::where('product_id', $product_id)->update(array('flag' => 0));

        if(!empty($categories) && count($categories) > 0) {
        	foreach ($categories as $category_id) {
        		$product_category = new ProductCategory;
	        	$product_category->product_id = $product->id;
	        	$product_category->category_id = $category_id;
	        	$product_category->save();
        	}
        }

        $message = 'Product Updated';
        return response()->json(compact('message'));
    }

    public function show($product_id, Request $request) {

        $product_id = (int) $product_id;
    
        $product = Product::where('id', $product_id)->where('flag', 1)->first();
        if (is_null($product)) {
            $message = 'Product not found';
            return response()->json(compact('message'), 404);
        }

        $product_detail = Product::getById($product->id);

        return response()->json($product_detail);

    }

    public function destroy($product_id, Request $request) {

        $product_id = (int) $product_id;
	
		$product = Product::where('id', $product_id)->where('flag', 1)->first();
        if (is_null($product)) {
            $message = 'Product not found';
            return response()->json(compact('message'), 404);
        }

        $product->flag = 0;
        $product->save();

        ProductCategory::where('product_id', $product_id)->update(array('flag' => 0));

        ProductColor::where('product_id', $product_id)->update(array('flag' => 0));

        $message = 'Product Deleted';
        return response()->json(compact('message'));
    }



}