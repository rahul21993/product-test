<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class ProductCategory extends Model
{
    protected $table = 'product_categories';
    protected $guarded = [];

    public static function getCategories($product_id){
		$query = DB::table('categories')
							->join('product_categories', 'categories.id', '=', 'product_categories.category_id')
							->where('product_categories.product_id',$product_id)
							->select('categories.name')
							->where('product_categories.flag', 1);
       
		$result = $query->get();

		$implode_categories = array();
        $category_type_json = json_decode(json_encode($result), true);
        foreach($category_type_json as $category_string)
                $implode_categories[] = implode(', ', $category_string);
        $categories = implode(', ', $implode_categories);
        
        return $categories; 
	}
}
