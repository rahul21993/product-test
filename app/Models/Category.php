<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class Category extends Model
{
    protected $table = 'categories';
    protected $guarded = [];

    public static function getData() {
		
		$query = DB::table('categories')
						->where('categories.flag', 1);
						
		$categories = $query->get();
						
		return $categories;
	}
}
