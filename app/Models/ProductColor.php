<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class ProductColor extends Model
{
    protected $table = 'product_colors';
    protected $guarded = [];

    public static function getColors($product_id){
		$query = DB::table('colors')
							->join('product_colors', 'colors.id', '=', 'product_colors.color_id')
							->where('product_colors.product_id',$product_id)
							->select('colors.name')
							->where('product_colors.flag', 1);
       
		$result = $query->get();

		$implode_colors = array();
        $color_type_json = json_decode(json_encode($result), true);
        foreach($color_type_json as $color_string)
                $implode_colors[] = implode(', ', $color_string);
        $colors = implode(', ', $implode_colors);
        
        return $colors; 
	}
}
