<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class Product extends Model
{
    protected $table = 'products';
    protected $guarded = [];

    public static function getData() {

		$query = DB::table('products')
								// ->leftJoin('apparel_sizes', function ($join) { $join->on('apparel_sizes.id', '=', 'products.apparel_size_id')->where('apparel_sizes.flag', 1); })
								->join('apparel_sizes', 'apparel_sizes.id', '=', 'products.apparel_size_id')
								->select('products.*', 'apparel_sizes.code')
								->where('products.flag', 1)
								->where('apparel_sizes.flag', 1);

		$result = $query->get();		
		foreach ($result as $row) {
			$row->categories = \App\Models\ProductCategory::getCategories($row->id);
			$row->colors = \App\Models\ProductColor::getColors($row->id);
		}

		return $result;

	}

	public static function getById($product_id) {
		
		$query = DB::table('products')
								->join('apparel_sizes', 'apparel_sizes.id', '=', 'products.apparel_size_id')
								->select('products.*', 'apparel_sizes.code')
								->where('products.flag', 1)
								->where('apparel_sizes.flag', 1)
								->where('products.id', $product_id);

		$result = $query->first();

		$result->categories = \App\Models\ProductCategory::getCategories($result->id);
		$result->colors = \App\Models\ProductColor::getColors($result->id);
				
		return $result;

	}
}
