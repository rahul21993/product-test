<?php

namespace App\Models;
use DB;

use Illuminate\Database\Eloquent\Model;

class Color extends Model
{
    protected $table = 'colors';
    protected $guarded = [];

    public static function getData() {
		
		$query = DB::table('colors')
						->where('colors.flag', 1);
						
		$colors = $query->get();
						
		return $colors;
	}

}
