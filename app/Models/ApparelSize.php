<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class ApparelSize extends Model
{
    protected $table = 'apparel_sizes';
    protected $guarded = [];

    public static function getData() {
		
		$query = DB::table('apparel_sizes')
						->where('apparel_sizes.flag', 1);
						
		$apparel_sizes = $query->get();
						
		return $apparel_sizes;
	}
}
