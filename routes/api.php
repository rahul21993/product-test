<?php


Route::group(['namespace' => 'API'], function() {

	Route::resource('color', 'ColorController');
	Route::resource('apparel_size', 'ApparelSizeController');
	Route::resource('category', 'CategoryController');
	Route::resource('product', 'ProductController');

});